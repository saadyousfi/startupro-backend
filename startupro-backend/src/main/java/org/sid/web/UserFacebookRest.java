package org.sid.web;


import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserFacebookRest {

    @RequestMapping(value = "/user/home",method = RequestMethod.POST)
    public Object getUser(@RequestParam(value = "token") String token){

        String access_token=token;
        Facebook facebook = new FacebookTemplate(access_token);
        return facebook.userOperations().getUserProfile();
    }
}
