package org.sid.web;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.sid.dao.RoleRepository;
import org.sid.entities.Role;
import org.sid.web.exception.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/users")
public class RoleRestService {
	
	@Autowired
	private RoleRepository roleRepository;
	
	@RequestMapping(value="/roles",method=RequestMethod.GET)
	public List<Role> getRoles(){
		return roleRepository.findAll();
	}
	
	@RequestMapping(value="/roles/{id}",method=RequestMethod.GET)
	public Optional<Role> Role(@PathVariable Long id){
		Optional<Role> role = roleRepository.findById(id);
		if(!role.isPresent()) throw new UserNotFoundException("id-" + id);
		return role;
	}
	
	
	@RequestMapping(value="/roles",method=RequestMethod.POST)
	public Role save(@Valid @RequestBody Role role){
		return roleRepository.save(role);
	}
	
	@RequestMapping(value="/roles/{id}",method=RequestMethod.DELETE)
	public boolean supprimer(@RequestBody Long id){
		roleRepository.deleteById(id);
		return true;
	}
	
	@RequestMapping(value="/roles/{id}",method=RequestMethod.PUT)
	public Role save(@PathVariable Long id,@Valid @RequestBody Role r){
		r.setId(id);
		return roleRepository.save(r);
	}

}
