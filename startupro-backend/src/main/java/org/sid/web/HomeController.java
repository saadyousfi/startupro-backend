package org.sid.web;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

     @RequestMapping("/")
     public String home(){
         return "Hello World!";
     }
     
     @RequestMapping("/api/articles")
     public String articles(){
         return "my articles!";
     }
 }