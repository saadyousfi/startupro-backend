package org.sid.web;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;

import org.sid.dao.RoleRepository;
import org.sid.dao.UserRepository;
import org.sid.entities.Role;
import org.sid.entities.User;
import org.sid.entities.VerificationToken;
import org.sid.events.OnRegistrationCompleteEvent;
import org.sid.service.EmailService;
import org.sid.service.UserServiceImpl;
import org.sid.validator.UserValidator;
import org.sid.web.exception.UserNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping("/api")
public class UserRestService {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
    private UserServiceImpl userService;

    @Autowired
    private UserValidator userValidator;
    
    @Autowired
    private EmailService emailService;
    
    @Autowired
    ApplicationEventPublisher eventPublisher;
    
    @Autowired
    private MessageSource messages;

    Logger logger = LoggerFactory.getLogger(UserRestService.class);

	
	@RequestMapping(value="/users",method=RequestMethod.GET)
	public List<User> getUsers(){
		return userRepository.findAll();
	}
	
	@RequestMapping(value="/users/by",method=RequestMethod.GET)
	public List<User> getUsersbyRole(@RequestParam(name="Role",defaultValue="") String nrole){
		List<User> users = new ArrayList<>();
		for(User u: userRepository.findAll()) {
			for(Role role: u.getRoles()) {
				if(role.getNom().equals(nrole)) users.add(u);
			}
		}
		return users;
	}

	@RequestMapping(value="/users/{id}",method=RequestMethod.GET)
	public Optional<User> User(@PathVariable Long id){
		Optional<User> user = userRepository.findById(id);
		if(!user.isPresent()) throw new UserNotFoundException("id-" + id);
		return user;
	}
	
	@RequestMapping(value="/users/chercher",method=RequestMethod.GET)
	public Page<User> chercher(
			@RequestParam(name="mU",defaultValue="") String mu,
			@RequestParam(name="page",defaultValue="0") int page,
			@RequestParam(name="size",defaultValue="5") int size){
		return userRepository.chercher("%"+mu+"%", PageRequest.of(page, size));
	}
	
	@RequestMapping(value="/users",method=RequestMethod.POST)
	public User save(@Valid @RequestBody User u){
		return userRepository.save(u);
	}
	
	@RequestMapping(value="/users/{id}",method=RequestMethod.DELETE)
	public boolean supprimer(@RequestBody Long id){
		userRepository.deleteById(id);
		return true;
	}
	
	@RequestMapping(value="/users/{id}",method=RequestMethod.PUT)
	public User save(@PathVariable Long id,@Valid @RequestBody User u){
		u.setId(id);
		return userRepository.save(u);
	}
	

	@RequestMapping(value = "/users/getInfos", method = RequestMethod.GET)
	public User getUserinfo() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User user =(User) authentication.getPrincipal();
		return user;
	}

	// LOGIN AND REGISTRATON
	

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registration(@Valid @ModelAttribute("userForm") User userForm, BindingResult bindingResult, WebRequest request, Model model) {

        userValidator.validate(userForm, bindingResult);
    	if (bindingResult.hasErrors()) {
    		return "Form not valid";
        }
         
        User registered = new User(userForm);
        try {
            String appUrl = request.getContextPath();
            eventPublisher.publishEvent(new OnRegistrationCompleteEvent
              (registered, request.getLocale(), appUrl));
        } catch (Exception me) {
    		return "Erreur email";
        }
		return "Success register";
        
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", "Your username and password is invalid.");

        if (logout != null)
            model.addAttribute("message", "You have been logged out successfully.");

        return "login";
    }
    
    // USER REGISTRATION CONFIRM
    
    @RequestMapping(value = "/registration/Confirm", method = RequestMethod.GET)
    public String confirmRegistration (WebRequest request, Model model, @RequestParam("token") String token) {
         
        VerificationToken verificationToken = userService.getVerificationToken(token);
        if (verificationToken == null) {
            return "token null";
        }
         
        User user = verificationToken.getUser();
        /*Calendar cal = Calendar.getInstance();
        if ((verificationToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
            return "token expired";
        } */
         
        user.setActive(true); 
        userService.saveRegisteredUser(user); 
        return "Account activated !"; 
    }
    
    // ACTIVATE USER ACCOUNT
    @RequestMapping(value = "/users/activate/{userId}", method = RequestMethod.PUT)
    public User activateAccount(@PathVariable Long userId){

        Optional<User> user = userRepository.findById(userId);

        if(user.isPresent()) {
            userRepository.save(user.get());
        }

        return user.get();
    }


}
