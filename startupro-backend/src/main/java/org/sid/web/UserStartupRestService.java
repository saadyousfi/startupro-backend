package org.sid.web;

import java.io.Console;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.sid.dao.User_StartupRepository;
import org.sid.entities.User;
import org.sid.entities.User_Startup;
import org.sid.web.exception.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/users")
public class UserStartupRestService {
	
	@Autowired
	private User_StartupRepository user_StartupRepository;
	
	@RequestMapping(value="/startup",method=RequestMethod.GET)
	public List<User_Startup> getUser_Startup(){
		return user_StartupRepository.findAll();
	}
	
	@RequestMapping(value="/startup/{id}",method=RequestMethod.GET)
	public Optional<User_Startup> User_Startup(@PathVariable Long id){
		Optional<User_Startup> user_Startup = user_StartupRepository.findById(id);
		if(!user_Startup.isPresent()) throw new UserNotFoundException("id-" + id);
		return user_Startup;
	}
	
	
	@RequestMapping(value="/startup",method=RequestMethod.POST)
	public User_Startup save(@Valid @RequestBody User_Startup user_Startup){
		return user_StartupRepository.save(user_Startup);
	}
	
	@RequestMapping(value="/startup/{id}",method=RequestMethod.DELETE)
	public boolean supprimer(@RequestBody Long id){
		user_StartupRepository.deleteById(id);
		return true;
	}
	
	@RequestMapping(value="/startup/{id}",method=RequestMethod.PUT)
	public User_Startup save(@PathVariable Long id,@Valid @RequestBody User_Startup us){
		us.setId(id);
		return user_StartupRepository.save(us);
	}
	
	@RequestMapping(value = "/startup/getInfos", method = RequestMethod.GET)
	public Optional<User_Startup> getUserStartupinfo() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User user =(User) authentication.getPrincipal();
		return user_StartupRepository.findByUserId(user);
	}


}
