package org.sid.validator;

import static org.assertj.core.api.Assertions.offset;

import org.sid.entities.Role;
import org.sid.entities.User;
import org.sid.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

@Component
public class UserValidator implements org.springframework.validation.Validator {

	@Autowired
    private UserServiceImpl userService;

    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
		User user = (User) o;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "NotEmpty");
		if (user.getUsername().length() < 6 || user.getUsername().length() > 32) {
		    errors.rejectValue("username", "Size.userForm.username");
		}
		else if (userService.findByUsername(user.getUsername()).isPresent()) {
		    errors.rejectValue("username", "Duplicate.userForm.username");
		}
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty");
		if (user.getPassword().length() < 8 || user.getPassword().length() > 255) {
		    errors.rejectValue("password", "Size.userForm.password");
		}
		else if (!user.getPasswordConfirm().equals(user.getPassword())) {
		    errors.rejectValue("passwordConfirm", "Diff.userForm.passwordConfirm");
	    }
		
	   
	   for (Role role : user.getRoles()) {
		   if(role.getNom().equals("ADMIN")) {
			   errors.rejectValue("roles", "user cannot be admin");
		   }
		   else if (!(role.getNom().equals("USER") || role.getNom().equals("STARTUP"))) {
	    	   errors.rejectValue("roles", "user is neither client or startup");
	       }
		   else if(role.getNom().equals("USER")) {
	    	   if(user.getActive()) errors.rejectValue("active", "user can't be active before mail check !");
	       }
		   else if(role.getNom().equals("STARTUP")) {
	    	   if(user.getActive()) errors.rejectValue("active", "user can't be active before mail check !");
	    	   if(user.getValid()) errors.rejectValue("valid", "startup account can't be valid before admin check !");
	       }
	       
	   }
        
    }

}
