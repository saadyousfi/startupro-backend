package org.sid.service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.sid.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
public class EmailService {

	@Autowired
    private JavaMailSender javaMailSender;

    public void sendMailConfirm(User user) throws MailException, MessagingException {

        MimeMessage mail=javaMailSender.createMimeMessage();
        MimeMessageHelper helper=new MimeMessageHelper(mail,true);

        helper.setTo(user.getEmail());
        helper.setFrom("medbencheikh95@gmail.com");
        helper.setSubject("STARTUPRO REGISTRATION");
        helper.setText("Hello "+user.getNom()+" "+user.getPrenom()+". Thank u for signing up in our application 'STARTUPRO'. Your registration was succeed."
        		+ " Please activate your account with this link below : "+"<br/><a href='http://localhost:4200/users/activate/"+user.getId()+"'>"
        				+ "Activate my account</a>",true);
        
        javaMailSender.send(mail);

    }
}
