package org.sid.service;

import java.util.Optional;

import org.sid.dao.UserRepository;
import org.sid.entities.User;
import org.sid.entities.CustomUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
    private UserRepository userRepository;
	
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<User> userOp = userRepository.findByUsername(username);

		userOp
			.orElseThrow(() -> new UsernameNotFoundException("Username not found!"));
		return userOp
				.map(CustomUserDetails::new)
				.get();
	}

}
