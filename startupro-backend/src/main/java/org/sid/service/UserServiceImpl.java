package org.sid.service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.sid.dao.RoleRepository;
import org.sid.dao.UserRepository;
import org.sid.dao.VerificationTokenRepository;
import org.sid.entities.Role;
import org.sid.entities.User;
import org.sid.entities.VerificationToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private PasswordEncoder bCryptPasswordEncoder;
    @Autowired
    private VerificationTokenRepository tokenRepository;

    @Override
    public void save(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRepository.save(user);
    }

    @Override
    public Optional<User> findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

	@Override
	public User getUser(String verificationToken) {
		User user = tokenRepository.findByToken(verificationToken).get().getUser();
        return user;
	}

	@Override
	public void saveRegisteredUser(User user) {
		userRepository.save(user);
		
	}

	@Override
	public void createVerificationToken(User user, String token) {
		VerificationToken myToken = new VerificationToken(token, user);
        tokenRepository.save(myToken);
		
	}

	@Override
	public VerificationToken getVerificationToken(String VerificationToken) {
		return tokenRepository.findByToken(VerificationToken).get();
	}

	@Override
	public User registerNewUserAccount(User accountDto) {
		User user = new User(accountDto);
        
		for(Role role: accountDto.getRoles()) {
			Optional<Role> roleUser;
			if(role.getNom() == "STARTUP") {
				roleUser = roleRepository.findById((long) 3);
			}else {
				roleUser = roleRepository.findById((long) 2);
			}
			if(roleUser.isPresent()) {
	        	Set<Role> roles = new HashSet<>();
	        	roles.add(roleUser.get());
	        	user.setRoles(roles);
	    	}
		}
		
        return userRepository.save(user);
	}

}
