package org.sid.service;

import java.util.Optional;

import org.sid.entities.User;
import org.sid.entities.VerificationToken;

public interface UserService {
	
	User registerNewUserAccount(User accountDto);
		 
	User getUser(String verificationToken);
	
	void saveRegisteredUser(User user);
	 
    void createVerificationToken(User user, String token);
 
    VerificationToken getVerificationToken(String VerificationToken);
    
	public void save(User user);
	
	public Optional<User> findByUsername(String username);
}
