package org.sid.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "list_amis")
public class List_Amis implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name ="id")
	private Long id;
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @OneToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    private User user;
    @Transient
    @OneToMany(targetEntity = DemandeAmi.class, fetch = FetchType.EAGER)
    private Set<DemandeAmi> demandes;
    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Set<DemandeAmi> getDemandes() {
		return demandes;
	}
	public void setDemandes(Set<DemandeAmi> demandes) {
		this.demandes = demandes;
	}
	public List_Amis() {
		super();
	}
	public List_Amis(Long id, User user) {
		super();
		this.id = id;
		this.user = user;
	}
	
    
    
}
