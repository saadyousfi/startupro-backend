package org.sid.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "consomation")
public class Consomation implements Serializable {

	@EmbeddedId
    protected ConsomationPK consomationPK;
	@Temporal(TemporalType.DATE)
	@Column(name ="date_debut")
	public Date date_debut;
	@Temporal(TemporalType.DATE)
	@Column(name ="date_fin")
	public Date date_fin;
    @Column(name = "rating")
	private int rating;
    @JoinColumn(name = "user_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    private Set<User> user;
    @JoinColumn(name = "service_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(targetEntity = Service.class, fetch = FetchType.EAGER)
    private Set<Service> services;
	public ConsomationPK getConsomationPK() {
		return consomationPK;
	}
	public void setConsomationPK(ConsomationPK consomationPK) {
		this.consomationPK = consomationPK;
	}
	public Date getDate_debut() {
		return date_debut;
	}
	public void setDate_debut(Date date_debut) {
		this.date_debut = date_debut;
	}
	public Date getDate_fin() {
		return date_fin;
	}
	public void setDate_fin(Date date_fin) {
		this.date_fin = date_fin;
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	public Set<User> getUser() {
		return user;
	}
	public void setUser(Set<User> user) {
		this.user = user;
	}
	public Set<Service> getServices() {
		return services;
	}
	public void setServices(Set<Service> services) {
		this.services = services;
	}
	public Consomation(ConsomationPK consomationPK, Date date_debut, Date date_fin, int rating, Set<User> user,
			Set<Service> services) {
		super();
		this.consomationPK = consomationPK;
		this.date_debut = date_debut;
		this.date_fin = date_fin;
		this.rating = rating;
		this.user = user;
		this.services = services;
	}
    
    
    
}
