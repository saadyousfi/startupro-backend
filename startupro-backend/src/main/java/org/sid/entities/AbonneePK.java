package org.sid.entities;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@Embeddable
public class AbonneePK implements Serializable {

	@Basic(optional = false)
    @NotNull
    @Column(name = "user_id")
    private Long userid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "list_id")
    private Long liststartupsid;
    
    public Long getUserid() {
		return userid;
	}

	public void setUserid(Long userid) {
		this.userid = userid;
	}

	public Long getListstartupsid() {
		return liststartupsid;
	}

	public void setListstartupsid(Long liststartupsid) {
		this.liststartupsid = liststartupsid;
	}

	public AbonneePK() {
		super();
	}
	public AbonneePK(@NotNull Long userid, @NotNull Long liststartupsid) {
		super();
		this.userid = userid;
		this.liststartupsid = liststartupsid;
	}

	@Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AbonneePK)) {
            return false;
        }
        AbonneePK other = (AbonneePK) object;
        if (this.userid != other.userid) {
            return false;
        }
        if (this.liststartupsid != other.liststartupsid) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.AbonneePK[ userid=" + userid + ", liststartupsid=" + liststartupsid + " ]";
    }
    
}
