package org.sid.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Type;

@Entity
@Table(name = "service")
public class Service implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
    @Size(max = 20)
    @Column(name = "nom")
	private String nom;
    @Size(max = 16777215)
    @Column(name = "description")
	private String description;
    @Column(name = "rate")
	private int rate;
	@Column(name="etat")
	@Type(type="yes_no")
	private Boolean etat;
	@Column(name="status")
	@Type(type="yes_no")
	private Boolean status;
	@Temporal(TemporalType.DATE)
	@Column(name ="dateconsommation")
	public Date dateConsommation;
	@Temporal(TemporalType.DATE)
	@Column(name ="dateactivation")
	public Date dateActivation;
    @JoinColumn(name = "startup_id", referencedColumnName = "id")
    @ManyToOne(targetEntity = User_Startup.class, fetch = FetchType.EAGER)
    private User_Startup startup;
	@Transient
	@ManyToMany(mappedBy = "services",cascade = CascadeType.MERGE)
	private Set<Publication> pubs;
    @Transient
    @OneToMany(targetEntity = Consomation.class, fetch = FetchType.EAGER)
    private Set<Consomation> consomations;
    @JoinColumn(name = "domaine_id", referencedColumnName = "id")
    @ManyToOne(targetEntity = Domaine.class, fetch = FetchType.EAGER)
    private Domaine domaine;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getRate() {
		return rate;
	}
	public void setRate(int rate) {
		this.rate = rate;
	}
	public Boolean getEtat() {
		return etat;
	}
	public void setEtat(Boolean etat) {
		this.etat = etat;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public Date getDateConsommation() {
		return dateConsommation;
	}
	public void setDateConsommation(Date dateConsommation) {
		this.dateConsommation = dateConsommation;
	}
	public Date getDateActivation() {
		return dateActivation;
	}
	public void setDateActivation(Date dateActivation) {
		this.dateActivation = dateActivation;
	}
	public User_Startup getStartup() {
		return startup;
	}
	public void setStartup(User_Startup startup) {
		this.startup = startup;
	}
	
	public Set<Publication> getPubs() {
		return pubs;
	}
	public void setPubs(Set<Publication> pubs) {
		this.pubs = pubs;
	}
	
	public Set<Consomation> getConsomations() {
		return consomations;
	}
	public void setConsomations(Set<Consomation> consomations) {
		this.consomations = consomations;
	}
	
	public Domaine getDomaine() {
		return domaine;
	}
	public void setDomaine(Domaine domaine) {
		this.domaine = domaine;
	}
	public Service() {
		super();
	}
	public Service(long id, @Size(max = 20) String nom, @Size(max = 16777215) String description, int rate,
			Boolean etat, Boolean status, Date dateConsommation, Date dateActivation, User_Startup startup
			, Domaine domaine) {
		super();
		this.id = id;
		this.nom = nom;
		this.description = description;
		this.rate = rate;
		this.etat = etat;
		this.status = status;
		this.dateConsommation = dateConsommation;
		this.dateActivation = dateActivation;
		this.startup = startup;
		this.domaine = domaine;
	}
	
    
	
}
