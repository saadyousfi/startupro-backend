package org.sid.entities;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import org.codehaus.jackson.annotate.JsonIgnore;

@Entity
@Table(name = "publication")
public class Publication implements Serializable {
	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name ="id")
	private Long id;
	@Column(name ="contenu")
    @Size(max = 6777215)
	private String contenu;
	@Temporal(TemporalType.DATE)
	@Column(name ="datepub")
	public Date datePub;
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @ManyToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    private User user;
	@Transient
    @OneToMany(targetEntity = Review.class, fetch = FetchType.EAGER)
    private Set<Review> cmts;
	@JsonIgnore	
	@ManyToMany(fetch = FetchType.EAGER,cascade = CascadeType.MERGE)
    @JoinTable(name = "pub_has_serv", joinColumns = @JoinColumn(name = "pub_id"), inverseJoinColumns = @JoinColumn(name = "serv_id"))
	private Set<Service> services;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getContenu() {
		return contenu;
	}
	public void setContenu(String contenu) {
		this.contenu = contenu;
	}
	public Date getDatePub() {
		return datePub;
	}
	public void setDatePub(Date datePub) {
		this.datePub = datePub;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Set<Review> getCmts() {
		return cmts;
	}
	public void setCmts(Set<Review> cmts) {
		this.cmts = cmts;
	}
	
	public Set<Service> getServices() {
		return services;
	}
	public void setServices(Set<Service> services) {
		this.services = services;
	}
	public Publication() {
		super();
	}
	public Publication(Long id, @Size(max = 6777215) String contenu, Date datePub, User user, Set<Review> cmts) {
		super();
		this.id = id;
		this.contenu = contenu;
		this.datePub = datePub;
		this.user = user;
		this.cmts = cmts;
	}
    
    
    
}
