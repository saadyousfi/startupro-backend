package org.sid.entities;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "abonnee")
public class Abonnee {


    @EmbeddedId
    protected AbonneePK abonneePKS;
	@Temporal(TemporalType.DATE)
	@Column(name ="date")
	public Date myDate;
    @JoinColumn(name = "user_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    private Set<User> user;
    @JoinColumn(name = "list_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(targetEntity = List_Startups.class, fetch = FetchType.EAGER)

	private Set<List_Startups> allListOfStartups;
    
	public AbonneePK getAbonneePK() {
		return abonneePK;
	}
	public void setAbonneePK(AbonneePK abonneePK) {
		this.abonneePK = abonneePK;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Set<User> getUser() {
		return user;
	}
	public void setUser(Set<User> user) {
		this.user = user;
	}
	public Set<List_Startups> getList_startups() {
		return list_startups;
	}
	public void setList_startups(Set<List_Startups> list_startups) {
		this.list_startups = list_startups;
	}
	public Abonnee() {
		super();
	}
	public Abonnee(AbonneePK abonneePK, Date date, Set<User> user, Set<List_Startups> list_startups) {
		super();
		this.abonneePK = abonneePK;
		this.date = date;
		this.user = user;
		this.list_startups = list_startups;
	}
    
}
