package org.sid.entities;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Type;


@Entity
@Table(name = "commentaire")
public class Review implements Serializable {

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name ="id")
	private Long id;
	@Column(name ="comment")
    @Size(max = 255)
	private String comment;
	@Temporal(TemporalType.DATE)
	@Column(name ="datecmt")
	public Date dateCmt;
	@Column(name="type")
    @Size(max = 20)
	private String type;
	@Column(name="valeur")
	@Type(type="yes_no")
	private Boolean valeur;
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @ManyToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    private User user;
    @JoinColumn(name = "pub_id", referencedColumnName = "id")
    @ManyToOne(targetEntity = Publication.class, fetch = FetchType.EAGER)
    private Publication pub;
    
    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.type = "COMMENT";
		this.comment = comment;
	}
	public Date getDateCmt() {
		return dateCmt;
	}
	public void setDateCmt(Date dateCmt) {
		this.dateCmt = dateCmt;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Boolean getValeur() {
		return valeur;
	}
	public void setValeur(Boolean valeur) {
		this.valeur = valeur;
	}
	public Publication getPub() {
		return pub;
	}
	public void setPub(Publication pub) {
		this.pub = pub;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public void Like() {
		this.type = "LIKE";
		this.valeur = true;
	}
	public void DissLike() {
		this.type = "LIKE";
		this.valeur = false;
	}
	public Review() {
		super();
	}
	public Review(Long id, @Size(max = 255) String comment, Date dateCmt, @Size(max = 20) String type,
			Boolean valeur, User user, Publication pub) {
		super();
		this.id = id;
		this.comment = comment;
		this.dateCmt = dateCmt;
		this.type = type;
		this.valeur = valeur;
		this.user = user;
		this.pub = pub;
	}
	
    
    
}
