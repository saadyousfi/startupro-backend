package org.sid.entities;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@Embeddable
public class ConsomationPK implements Serializable {
	
	@Basic(optional = false)
    @NotNull
    @Column(name = "user_id")
    private Long userid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "service_id")
    private Long serviceid;
    
    public ConsomationPK() {
    	super();
    }
    
	public ConsomationPK(@NotNull Long userid, @NotNull Long serviceid) {
		super();
		this.userid = userid;
		this.serviceid = serviceid;
	}

	public Long getUserid() {
		return userid;
	}

	public void setUserid(Long userid) {
		this.userid = userid;
	}

	public Long getServiceid() {
		return serviceid;
	}

	public void setServiceid(Long serviceid) {
		this.serviceid = serviceid;
	}

	@Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ConsomationPK)) {
            return false;
        }
        ConsomationPK other = (ConsomationPK) object;
        if (this.userid != other.userid) {
            return false;
        }
        if (this.serviceid != other.serviceid) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.AbonneePK[ userid=" + userid + ", serviceid=" + serviceid + " ]";
    }

}
