package org.sid.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Type;

@Entity
@Table(name = "demande_ami")
public class DemandeAmi implements Serializable {

	@EmbeddedId
    protected DemandeAmiPK demandeamiPK;
	@Temporal(TemporalType.DATE)
	@Column(name ="date")
	public Date date;
	@Column(name="etat")
	@Type(type="yes_no")
	private Boolean etat;
    @JoinColumn(name = "user_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    private Set<User> user;
    @JoinColumn(name = "list_id", referencedColumnName = "id",insertable = false, updatable = false)
    @ManyToOne(targetEntity = List_Amis.class, fetch = FetchType.EAGER)
    private Set<List_Amis> list_amis;

    
	public DemandeAmiPK getDemandeamiPK() {
		return demandeamiPK;
	}
	public void setDemandeamiPK(DemandeAmiPK demandeamiPK) {
		this.demandeamiPK = demandeamiPK;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Boolean getEtat() {
		return etat;
	}
	public void setEtat(Boolean etat) {
		this.etat = etat;
	}
	public Set<User> getUser() {
		return user;
	}
	public void setUser(Set<User> user) {
		this.user = user;
	}
	
	public Set<List_Amis> getList_amis() {
		return list_amis;
	}
	public void setList_amis(Set<List_Amis> list_amis) {
		this.list_amis = list_amis;
	}

	public DemandeAmi() {
		super();
	}
	public DemandeAmi(DemandeAmiPK demandeamiPK, Date date, Boolean etat, Set<User> user, Set<List_Amis> list_amis) {
		super();
		this.demandeamiPK = demandeamiPK;
		this.date = date;
		this.etat = etat;
		this.user = user;
		this.list_amis = list_amis;
	}

    
    
}
