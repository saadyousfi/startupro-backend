package org.sid.entities;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "list_startups")
public class List_Startups {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name ="id")
	private Long id;
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @OneToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    private User user;
    @Transient
    @OneToMany(targetEntity = Abonnee.class, fetch = FetchType.EAGER)
    private Set<Abonnee> abonnees;
    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Set<Abonnee> getAbonnees() {
		return abonnees;
	}
	public void setAbonnees(Set<Abonnee> abonnees) {
		this.abonnees = abonnees;
	}
	public List_Startups() {
		super();
	}
	public List_Startups(Long id, User user) {
		super();
		this.id = id;
		this.user = user;
	}
	
    
    
}
