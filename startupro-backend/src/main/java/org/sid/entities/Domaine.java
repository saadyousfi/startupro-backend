package org.sid.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
@Table(name ="domaine") 
public class Domaine implements Serializable {
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name ="id")
	private Long id;
	@NotEmpty
	@Column(unique = true, nullable = false, name ="domaine")
    @Size(max = 20)
	private String domaine;
    @Transient
    @OneToMany(targetEntity = Service.class, fetch = FetchType.EAGER)
    private Set<Service> services;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDomaine() {
		return domaine;
	}
	public void setDomaine(String domaine) {
		this.domaine = domaine;
	}
	public Set<Service> getServices() {
		return services;
	}
	public void setServices(Set<Service> services) {
		this.services = services;
	}
	public Domaine(String domaine) {
		super();
		this.domaine = domaine;
	}
	
	public Domaine() {
		super();
	}
	
	public Domaine(Long id, @NotEmpty @Size(max = 20) String domaine, Set<Service> services) {
		super();
		this.id = id;
		this.domaine = domaine;
		this.services = services;
	}
    
    
}
