package org.sid.entities;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@Embeddable
public class DemandeAmiPK implements Serializable {

	@Basic(optional = false)
    @NotNull
    @Column(name = "user_id")
    private Long userid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "list_id")
    private Long listamisid;
    
    public Long getUserid() {
		return userid;
	}

	public void setUserid(Long userid) {
		this.userid = userid;
	}

	public Long getListamisid() {
		return listamisid;
	}

	public void setListamisid(Long listamisid) {
		this.listamisid = listamisid;
	}

	public DemandeAmiPK() {
		super();
	}

	public DemandeAmiPK(@NotNull Long userid, @NotNull Long listamisid) {
		super();
		this.userid = userid;
		this.listamisid = listamisid;
	}

	@Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DemandeAmiPK)) {
            return false;
        }
        DemandeAmiPK other = (DemandeAmiPK) object;
        if (this.userid != other.userid) {
            return false;
        }
        if (this.listamisid != other.listamisid) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.DemandeAmiPK[ userid=" + userid + ", listamisid=" + listamisid + " ]";
    }
    
}
