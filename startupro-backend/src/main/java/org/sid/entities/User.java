package org.sid.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.Type;

@Entity
@Table(name ="user") 
public class User implements Serializable {

	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name ="id")
	private Long id;
	@NotEmpty
	@Column(unique = true, nullable = false, name ="username")
    @Size(max = 45, min = 2, message="Le nom doit avoir au moin 2 charactères")
	private String username;
	@NotEmpty
	@Column(name ="password")
    @Size(max = 255)
	private String password;
	@Transient
	private String passwordConfirm;
	@Column(name ="nom")
    @Size(max = 25)
	private String nom;
	@Column(name ="prenom")
    @Size(max = 25)
	private String prenom;
	@Temporal(TemporalType.DATE)
	@Column(name ="datenaissance")
	public Date dateNaissance;
	@NotEmpty
	@Email
	@Column(unique = true, nullable = false, name="email")
    @Size(max = 60)
	private String email;
	@Column(name ="photo")
	private String photo;
	@Column(name="tel")
    @Size(max = 25)
	private String tel;
	@Column(name="active")
	@Type(type="yes_no")
	private Boolean active;
	@Column(name="valid")
	@Type(type="yes_no")
	private Boolean valid;
	@JsonIgnore	
	@ManyToMany(fetch = FetchType.EAGER,cascade = CascadeType.MERGE)
    @JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
	private Set<Role> roles;
	@Transient
	@OneToOne(cascade = CascadeType.ALL, mappedBy = "userId")
    private User_Startup user_Startup;
	@Transient
	@OneToOne(cascade = CascadeType.ALL, mappedBy = "user")
	private VerificationToken verificationToken;
	@Transient
	@OneToOne(cascade = CascadeType.ALL, mappedBy = "user")
	private List_Startups list_startups;
	@Transient
	@OneToOne(cascade = CascadeType.ALL, mappedBy = "user")
	private List_Amis list_amis;
    @Transient
    @OneToMany(targetEntity = DemandeAmi.class, fetch = FetchType.EAGER)
    private Set<DemandeAmi> demandes;
    @Transient
    @OneToMany(targetEntity = Abonnee.class, fetch = FetchType.EAGER)
    private Set<Abonnee> abonnees;
    @Transient
    @OneToMany(targetEntity = Message.class, fetch = FetchType.EAGER)
    private Set<Message> sent_msgs;
    @Transient
    @OneToMany(targetEntity = Message.class, fetch = FetchType.EAGER)
    private Set<Message> rec_msgs;
    @Transient
    @OneToMany(targetEntity = Publication.class, fetch = FetchType.EAGER)
    private Set<Publication> publications;
    @Transient
    @OneToMany(targetEntity = Review.class, fetch = FetchType.EAGER)
    private Set<Review> commentaires;
    @Transient
    @OneToMany(targetEntity = Consomation.class, fetch = FetchType.EAGER)
    private Set<Consomation> consomations;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPasswordConfirm() {
		return passwordConfirm;
	}
	public void setPasswordConfirm(String passwordConfirm) {
		this.passwordConfirm = passwordConfirm;
	}
	public Set<Role> getRoles() {
		return roles;
	}
	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
	public void addRole(Long id) {
		this.roles.add(new Role(id));
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public Date getDateNaissance() {
		return dateNaissance;
	}
	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	public Boolean getValid() {
		return valid;
	}
	public void setValid(Boolean valid) {
		this.valid = valid;
	}
	public User_Startup getUser_Startup() {
		return user_Startup;
	}
	public void setUser_Startup(User_Startup user_Startup) {
		this.user_Startup = user_Startup;
	}
	public VerificationToken getVerificationToken() {
		return verificationToken;
	}
	public void setVerificationToken(VerificationToken verificationToken) {
		this.verificationToken = verificationToken;
	}
	public List_Startups getList_startups() {
		return list_startups;
	}
	public void setList_startups(List_Startups list_startups) {
		this.list_startups = list_startups;
	}
	public List_Amis getList_amis() {
		return list_amis;
	}
	public void setList_amis(List_Amis list_amis) {
		this.list_amis = list_amis;
	}
	public Set<DemandeAmi> getDemandes() {
		return demandes;
	}
	public void setDemandes(Set<DemandeAmi> demandes) {
		this.demandes = demandes;
	}
	public Set<Abonnee> getAbonnees() {
		return abonnees;
	}
	public void setAbonnees(Set<Abonnee> abonnees) {
		this.abonnees = abonnees;
	}
	public Set<Message> getSent_msgs() {
		return sent_msgs;
	}
	public void setSent_msgs(Set<Message> sent_msgs) {
		this.sent_msgs = sent_msgs;
	}
	public Set<Message> getRec_msgs() {
		return rec_msgs;
	}
	public void setRec_msgs(Set<Message> rec_msgs) {
		this.rec_msgs = rec_msgs;
	}
	public Set<Publication> getPublications() {
		return publications;
	}
	public void setPublications(Set<Publication> publications) {
		this.publications = publications;
	}
	public Set<Review> getCommentaires() {
		return commentaires;
	}
	public void setCommentaires(Set<Review> commentaires) {
		this.commentaires = commentaires;
	}
	
	public Set<Consomation> getConsomations() {
		return consomations;
	}
	public void setConsomations(Set<Consomation> consomations) {
		this.consomations = consomations;
	}
	public User() {
		super();
		this.active = false;
		this.valid = false;
	}
	public User(User user) {
		super();
		this.id = user.getId();
		this.username = user.getUsername();
		this.password = user.getPassword();
		this.passwordConfirm = user.getPasswordConfirm();
		this.roles = user.getRoles();
		this.nom = user.getNom();
		this.prenom = user.getPrenom();
		this.dateNaissance = user.getDateNaissance();
		this.email = user.getEmail();
		this.photo = user.getPhoto();
		this.tel = user.getTel();
		this.active = user.getActive();
		this.valid = user.getValid();
		this.user_Startup = user.getUser_Startup();
		this.verificationToken = user.getVerificationToken();
	}
	
}
