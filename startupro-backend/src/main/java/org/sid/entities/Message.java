package org.sid.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Type;

@Entity
@Table(name="message")
public class Message implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name ="id")
	private Long id;
	@Column(name ="message")
    @Size(max = 255)
	private String message;
	@Column(name="vue")
	@Type(type="yes_no")
	private Boolean vue;
	@Temporal(TemporalType.DATE)
	@Column(name ="datemsg")
	public Date dateMsg;
    @JoinColumn(name = "user_sender_id", referencedColumnName = "id")
    @ManyToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    private User user_sender;
    @JoinColumn(name = "user_receiver_id", referencedColumnName = "id")
    @ManyToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    private User user_receiver;
    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public User getUser_sender() {
		return user_sender;
	}
	public void setUser_sender(User user_sender) {
		this.user_sender = user_sender;
	}
	public User getUser_receiver() {
		return user_receiver;
	}
	public void setUser_receiver(User user_receiver) {
		this.user_receiver = user_receiver;
	}
	public Boolean getVue() {
		return vue;
	}
	public void setVue(Boolean vue) {
		this.vue = vue;
	}
	public Date getDateMsg() {
		return dateMsg;
	}
	public void setDateMsg(Date dateMsg) {
		this.dateMsg = dateMsg;
	}
	public Message() {
		super();
	}
	public Message(Long id, @Size(max = 255) String message, Boolean vue, Date dateMsg, User user_sender,
			User user_receiver) {
		super();
		this.id = id;
		this.message = message;
		this.vue = vue;
		this.dateMsg = dateMsg;
		this.user_sender = user_sender;
		this.user_receiver = user_receiver;
	}
	
	
    
	
    
}
