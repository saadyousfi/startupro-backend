package org.sid.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

@Entity
@Table(name = "user_startup")
public class User_Startup implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
    @Size(max = 20)
    @Column(name = "nom")
	private String nom;
    @Size(max = 45)
    @Column(name = "site_web")
	private String site_web;
    @Size(max = 2)
    @Column(name = "pays")
	private String pays;
    @Size(max = 20)
    @Column(name = "ville")
	private String ville;
	@Lob
    @Size(max = 6777215)
    @Column(name = "address")
	private String address;
	@Column(name="tel")
    @Size(max = 25)
	private String tel;
	@Lob
    @Size(max = 16777215)
    @Column(name = "description")
	private String description;
    @Lob
    @Column(name = "logo")
    private byte[] logo;
	@Temporal(TemporalType.DATE)
	@Column(name ="datecreation")
	private Date date_creation;
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @OneToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    private User userId;
	@Transient
    @OneToMany(targetEntity = Service.class, fetch = FetchType.EAGER)
    private Set<Service> services;
    
	public User_Startup() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	
	public User_Startup(long id, @Size(max = 20) String nom, @Size(max = 45) String site_web,
			@Size(max = 2) String pays, @Size(max = 20) String ville, @Size(max = 6777215) String address,
			@Size(max = 25) String tel, @Size(max = 16777215) String description, byte[] logo, Date date_creation,
			User userId, Set<Service> services) {
		super();
		this.id = id;
		this.nom = nom;
		this.site_web = site_web;
		this.pays = pays;
		this.ville = ville;
		this.address = address;
		this.tel = tel;
		this.description = description;
		this.logo = logo;
		this.date_creation = date_creation;
		this.userId = userId;
		this.services = services;
	}

	public Set<Service> getServices() {
		return services;
	}
	public void setServices(Set<Service> services) {
		this.services = services;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getSite_web() {
		return site_web;
	}
	public void setSite_web(String site_web) {
		this.site_web = site_web;
	}
	public String getPays() {
		return pays;
	}
	public void setPays(String pays) {
		this.pays = pays;
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public byte[] getLogo() {
		return logo;
	}
	public void setLogo(byte[] logo) {
		this.logo = logo;
	}
	public Date getDate_creation() {
		return date_creation;
	}
	public void setDate_creation(Date date_creation) {
		this.date_creation = date_creation;
	}
	public User getUserId() {
		return userId;
	}
	public void setUserId(User userId) {
		this.userId = userId;
	}
    
    
}
