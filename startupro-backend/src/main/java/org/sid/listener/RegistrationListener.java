package org.sid.listener;

import java.util.UUID;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.sid.entities.User;
import org.sid.events.OnRegistrationCompleteEvent;
import org.sid.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

@Component
public class RegistrationListener implements ApplicationListener<OnRegistrationCompleteEvent> {
  
    @Autowired
    private UserServiceImpl service;
  
    @Autowired
    private JavaMailSender mailSender;
 
    @Override
    public void onApplicationEvent(OnRegistrationCompleteEvent event) {
        try {
			this.confirmRegistration(event);
		} catch (MailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
 
    private void confirmRegistration(OnRegistrationCompleteEvent event) throws MailException, MessagingException {
        User user = event.getUser();
        String token = UUID.randomUUID().toString();
        service.createVerificationToken(user, token);
         
        String recipientAddress = user.getEmail();
        String subject = "STARTUP Registration Confirmation";
        String confirmationUrl 
          = "localhost:8080/api/registration/Confirm?token=" + token;
         
        MimeMessage mail = mailSender.createMimeMessage();
        MimeMessageHelper helper=new MimeMessageHelper(mail,true);

        helper.setTo(recipientAddress);
        helper.setSubject(subject);
        helper.setText("Hello "+user.getNom()+" "+user.getPrenom()+". Thank u for signing up in our application 'STARTUPRO'. Your registration was succeed."
        		+ " Please activate your account with this link below : "+"<br/><a href='"+confirmationUrl+"' "+user.getId()+"'>"
        				+ "Activate my account</a>",true);
        mailSender.send(mail);
        
    }
}