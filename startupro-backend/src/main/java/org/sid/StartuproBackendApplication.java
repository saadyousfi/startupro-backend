package org.sid;

import static org.hamcrest.CoreMatchers.nullValue;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Set;

import org.sid.dao.DomaineRepository;
import org.sid.dao.List_AmisRepository;
import org.sid.dao.List_StartupsRepository;
import org.sid.dao.RoleRepository;
import org.sid.dao.UserRepository;
import org.sid.dao.User_StartupRepository;
import org.sid.entities.User;
import org.sid.entities.User_Startup;
import org.sid.entities.Domaine;
import org.sid.entities.List_Amis;
import org.sid.entities.List_Startups;
import org.sid.entities.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
public class StartuproBackendApplication implements CommandLineRunner {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private User_StartupRepository userStartupRepository;

	@Autowired
	private List_AmisRepository list_AmisRepository;

	@Autowired
	private List_StartupsRepository list_StartupsRepository;

	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private DomaineRepository domaineRepository;
	
	@Autowired
    private PasswordEncoder oauthClientPasswordEncoder;
	
	public static void main(String[] args) {
		SpringApplication.run(StartuproBackendApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		//DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		/*adminRepository.save(new Admin("Hassani", "Mohamed", df.parse("12/01/1998"), "hassan@gmail.com", "hassan.jpeg", "0622705740"));
		adminRepository.save(new Admin("Ibrahimi", "Khalid", df.parse("17/01/1998"), "khalid@gmail.com", "khalid.jpeg", "0622758930"));
		adminRepository.save(new Admin("Zellou", "Khadija", df.parse("12/02/1998"), "khadija@gmail.com", "khadija.jpeg", "0666755770"));
		adminRepository.findAll().forEach(c->{
			System.out.println(c.getNom());
		}); */
		//System.out.println(adminRepository.findByUsername("admin").getPassword());

		System.out.println(userRepository.findAll());
		
	}
	
	@Autowired
	public void authenticationManager(AuthenticationManagerBuilder builder, UserRepository repo) throws Exception {
		if(repo.count() == 0) {
			roleRepository.save(new Role("ADMIN"));
			roleRepository.save(new Role("USER"));
			roleRepository.save(new Role("STARTUP"));
			
			domaineRepository.save(new Domaine("INFORMATIQUE"));
			domaineRepository.save(new Domaine("INDUSTRIEL"));
			domaineRepository.save(new Domaine("MEDICAL"));
			domaineRepository.save(new Domaine("ELECTRIQUE"));
			domaineRepository.save(new Domaine("CIVILE"));
			
			User user = new User();
			
			Set<Role> roles = new HashSet<Role>();
			roles.add(new Role((long) 1));

			user.setActive(true);
			user.setValid(true);
			user.setEmail("admin@Email.com");
			user.setNom("YOUSFI");
			user.setPrenom("Saad");
			user.setRoles(roles);
			user.setUsername("admin");
			user.setPassword(oauthClientPasswordEncoder.encode("1234"));
			
			repo.save(user);
			list_AmisRepository.save(new List_Amis(null, user));
			list_StartupsRepository.save(new List_Startups(null, user));
		}
		
		if(repo.count() <= 1) {
			User user = new User();
			Set<Role> roles = new HashSet<Role>();
			roles.add(new Role((long) 2));

			user.setActive(true);
			user.setValid(true);
			user.setEmail("user@Email.com");
			user.setNom("USER_nom");
			user.setPrenom("User_prenom");
			user.setRoles(roles);
			user.setUsername("user");
			user.setPassword(oauthClientPasswordEncoder.encode("1234"));
			
			repo.save(user);
			list_AmisRepository.save(new List_Amis(null, user));
			list_StartupsRepository.save(new List_Startups(null, user));
		}
		
		if(repo.count() <= 2) {
			User user = new User();
			Set<Role> roles = new HashSet<Role>();
			roles.add(new Role((long) 3));
			
			
			user.setActive(true);
			user.setValid(true);
			user.setEmail("startup@Email.com");
			user.setNom("STARTUP_nom");
			user.setPrenom("STARTUP_prenom");
			user.setRoles(roles);
			user.setUsername("startup");
			user.setPassword(oauthClientPasswordEncoder.encode("1234"));

			User_Startup user_Startup = new User_Startup();
			DateFormat format = new SimpleDateFormat ("yyyy/MM/dd");
			user_Startup.setAddress("8,Avenue Ouezzane");
			user_Startup.setDate_creation(format.parse("2014/12/15"));
			user_Startup.setDescription("Description du startup");
			user_Startup.setNom("STARTUPRO");
			user_Startup.setPays("ma");
			user_Startup.setSite_web("startupro.com");
			user_Startup.setTel("0622705740");
			user_Startup.setVille("Rabat");
			user_Startup.setUserId(user);
			
			repo.save(user);
			list_AmisRepository.save(new List_Amis(null, user));
			list_StartupsRepository.save(new List_Startups(null, user));
			userStartupRepository.save(user_Startup);
		}
		
		if(repo.count() <= 3) {
			User user = new User();
			Set<Role> roles = new HashSet<>();
			roles.add(new Role((long) 2));

			user.setActive(false);
			user.setValid(false);
			user.setEmail("user2@Email.com");
			user.setNom("USER2_nom");
			user.setPrenom("USER2_prenom");
			user.setRoles(roles);
			user.setUsername("user2");
			user.setPassword(oauthClientPasswordEncoder.encode("123456"));
			
			repo.save(user);
			list_AmisRepository.save(new List_Amis(null, user));
			list_StartupsRepository.save(new List_Startups(null, user));
		}
		
		if(repo.count() <= 4) {
			User user = new User();
			Set<Role> roles = new HashSet<>();
			roles.add(new Role((long) 1));
			roles.add(new Role((long) 2));

			user.setActive(true);
			user.setValid(true);
			user.setEmail("admin_user@Email.com");
			user.setNom("ADMIN_USER_nom");
			user.setPrenom("ADMIN_USER_prenom");
			user.setRoles(roles);
			user.setUsername("admin_user");
			user.setPassword(oauthClientPasswordEncoder.encode("123456"));
			
			repo.save(user);
			list_AmisRepository.save(new List_Amis(null, user));
			list_StartupsRepository.save(new List_Startups(null, user));
		}
		
	}
}
