package org.sid.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

	
	public static final String RESOURCE_ID = "startupro";
    //public static final String SECURED_READ_SCOPE = "#oauth2.hasScope('read')";
    //public static final String SECURED_WRITE_SCOPE = "#oauth2.hasScope('write')";
    private static final String SECURED_PATTERN = "/api/**";
    private static final String LOGIN_SECURED_PATTERN = "/api/login";
    private static final String REGISTRATION_SECURED_PATTERN = "/api/registration/**";
    private static final String ARTICLES_SECURED_PATTERN = "/api/articles/**";
    private static final String USERS_SECURED_PATTERN = "/api/users/**";
    private static final String USERS_ACTIVATE_SECURED_PATTERN = "/api/users/activate/**";
    private static final String USERS_INFOS_SECURED_PATTERN = "/api/users/getInfos";
    private static final String STARTUP_INFOS_SECURED_PATTERN = "/api/users/startup/getInfos";
    private static final String HASROLE_ADMIN = "hasRole('ADMIN')";
    private static final String HASROLE_USER = "hasRole('USER')";
    private static final String HASROLE_STARTUP = "hasRole('STARTUP')";
    

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
        		/*.anonymous().disable()
        		.requestMatchers().antMatchers(SECURED_PATTERN)
                .and()*/
                .authorizeRequests()
    			.antMatchers("/","/home","/login","/oauth/**",LOGIN_SECURED_PATTERN,REGISTRATION_SECURED_PATTERN).permitAll()
                .antMatchers(HttpMethod.GET,ARTICLES_SECURED_PATTERN).access(HASROLE_ADMIN + " or " + HASROLE_USER + " or " + HASROLE_STARTUP)
                .antMatchers(HttpMethod.GET,USERS_INFOS_SECURED_PATTERN).access(HASROLE_ADMIN + " or " + HASROLE_USER + " or " + HASROLE_STARTUP)
                .antMatchers(USERS_ACTIVATE_SECURED_PATTERN).access(HASROLE_ADMIN + " or " + HASROLE_USER + " or " + HASROLE_STARTUP)
                .antMatchers(STARTUP_INFOS_SECURED_PATTERN).access(HASROLE_ADMIN + " or " + HASROLE_STARTUP)
                .antMatchers(USERS_SECURED_PATTERN).access(HASROLE_ADMIN)
                .anyRequest().denyAll();

    }
    
    
    
    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
         resources.resourceId(RESOURCE_ID);
    }
	
	
}
