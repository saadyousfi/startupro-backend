package org.sid.config;

import java.util.HashMap;
import java.util.Map;

import org.sid.entities.CustomUserDetails;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

public class CustomTokenEnhancer implements TokenEnhancer {

	@Override
	public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
		CustomUserDetails user = (CustomUserDetails) authentication.getPrincipal();
        final Map<String, Object> additionalInfo = new HashMap<>();

        additionalInfo.put("id", user.getId());
        additionalInfo.put("username", user.getUsername());
        additionalInfo.put("email", user.getEmail());
        additionalInfo.put("nom", user.getNom());
        additionalInfo.put("prenom", user.getPrenom());
        additionalInfo.put("role", user.getRoles());
        additionalInfo.put("datenaissance", user.getDateNaissance());
        additionalInfo.put("photo", user.getPhoto());
        additionalInfo.put("tel", user.getTel());

        ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);

        return accessToken;
	}

}
