package org.sid.dao;

import java.util.Optional;

import org.sid.entities.List_Amis;
import org.springframework.data.jpa.repository.JpaRepository;

public interface List_AmisRepository extends JpaRepository<List_Amis, Long> {

	Optional<List_Amis> findByUserId(List_Amis list_Amis);
}
