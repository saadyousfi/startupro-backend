package org.sid.dao;

import java.util.Optional;

import org.sid.entities.User;
import org.sid.entities.User_Startup;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface User_StartupRepository extends JpaRepository<User_Startup, Long> {

	@Query("select us from User_Startup us where us.nom like :x")
	public Page<User_Startup> chercher(@Param("x") String mus,Pageable pageable);

	Optional<User_Startup> findByNom(String nom);
	
	Optional<User_Startup> findByUserId(User user);
}
