package org.sid.dao;

import java.util.Optional;

import org.sid.entities.List_Startups;
import org.springframework.data.jpa.repository.JpaRepository;

public interface List_StartupsRepository extends JpaRepository<List_Startups, Long>  {
	
	Optional<List_Startups> findByUserId(List_Startups list_Startups);

}
