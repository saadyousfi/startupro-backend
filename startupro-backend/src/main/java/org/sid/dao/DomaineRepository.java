package org.sid.dao;

import org.sid.entities.Domaine;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface DomaineRepository extends JpaRepository<Domaine, Long> {

	@Query("select d from Domaine d where d.domaine like :x")
	public Page<Domaine> chercher(@Param("x") String md,Pageable pageable);
	
	Domaine findByDomaine(String domaine);
	
}
