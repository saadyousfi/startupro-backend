package org.sid.dao;

import java.util.Optional;

import org.sid.entities.User;
import org.sid.entities.VerificationToken;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VerificationTokenRepository extends JpaRepository<VerificationToken, Long> {
 
	Optional<VerificationToken> findByToken(String token);
 
	Optional<VerificationToken> findByUser(User user);
}