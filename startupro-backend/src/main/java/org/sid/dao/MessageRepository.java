package org.sid.dao;

import org.sid.entities.Message;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MessageRepository extends JpaRepository<Message, Long> {

	@Query("select m from Message m where m.vue like :x")
	public Page<Message> chercher(@Param("x") String mm,Pageable pageable);
	
	Message findByVue(Boolean vue);
}
