package org.sid.dao;

import org.sid.entities.DemandeAmi;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface DemandeAmiRepository extends JpaRepository<DemandeAmi, Long> {
	
	@Query("select d from DemandeAmi d where d.etat = :x")
	public Page<DemandeAmi> chercher(@Param("x") Boolean md,Pageable pageable);
	
	DemandeAmi findByEtat(Boolean etat);

}
