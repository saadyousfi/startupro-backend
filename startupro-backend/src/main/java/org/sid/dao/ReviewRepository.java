package org.sid.dao;

import org.sid.entities.Review;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ReviewRepository extends JpaRepository<Review, Long> {

	@Query("select r from Review r where r.type like :x")
	public Page<Review> chercher(@Param("x") String mr,Pageable pageable);
	
	Review findByType(String type);
}
