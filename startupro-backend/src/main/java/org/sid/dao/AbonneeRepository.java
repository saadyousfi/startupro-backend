package org.sid.dao;

import org.sid.entities.Abonnee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AbonneeRepository extends JpaRepository<Abonnee, Long>{
	
}
